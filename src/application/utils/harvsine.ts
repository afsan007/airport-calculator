/*
  Distance:
    Upon selecting two airports, the distance between the airports is calculated using the Haversine formula.
*/
export const haversine = (
  startLat: number,
  startLng: number,
  endLat: number,
  endLng: number
): string => {
  const latDiff = (startLat - endLat) * (Math.PI / 180);
  const lngDiff = (startLng - endLng) * (Math.PI / 180);
  const startLatRad = startLat * (Math.PI / 180);
  const endLatRad = endLat * (Math.PI / 180);

  const a =
    Math.sin(latDiff / 2) * Math.sin(Math.sin(latDiff / 2)) +
    Math.cos(startLatRad) *
      Math.cos(endLatRad) *
      Math.sin(lngDiff / 2) *
      Math.sin(lngDiff / 2);
  const c = Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)) * 2;
  return (3440 * c).toFixed(2);
};
