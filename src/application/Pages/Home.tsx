import { Grid } from '@mui/material';
import { NextPage } from 'next';
import dynamic from 'next/dynamic';
import { useState } from 'react';
import styled from '@emotion/styled';
import { AirportAutocomplete } from './Home/AirportAutocomplete';
import { Distance } from './Home/Distance';

// const GoogleMap = dynamic(() => import('./Home/GoogleMap'), {
//   ssr: false,
// });

const LeafletMap = dynamic(() => import('./Home/LeafletMap'), {
  ssr: false,
});
export interface OptionType {
  name: string;
  iata: string;
  city: string;
  state: string;
  lat: string;
  lng: string;
  size: string;
}

export const Home: NextPage = () => {
  const [originAirport, setOriginAirport] = useState<OptionType | undefined>(
    undefined
  );
  const [destinationAirport, setDestinationAirport] = useState<
    OptionType | undefined
  >(undefined);

  return (
    <Grid container direction='column'>
      <Grid
        item
        container
        gap={3}
        sx={{ p: { xs: 2, md: 6 }, px: { md: 0 } }}
        justifyContent='space-evenly'
      >
        <Grid item xs={12} md={5}>
          <StyledAirportAutocomplete
            onSelect={(val) => setOriginAirport(val)}
            disabledOption={destinationAirport}
            inputPlaceHolder='Origin airport'
          />
        </Grid>
        <Grid item xs={12} md={5}>
          <StyledAirportAutocomplete
            onSelect={(val) => setDestinationAirport(val)}
            disabledOption={originAirport}
            inputPlaceHolder='Destination airport'
          />
        </Grid>
      </Grid>
      <Grid item>
        {destinationAirport && originAirport && (
          <Distance
            originAirport={originAirport}
            destinationAirport={destinationAirport}
          />
        )}
      </Grid>
      <Grid item sx={{ flex: 1 }}>
        {/* <GoogleMap
          originAirport={originAirport}
          destinationAirport={destinationAirport}
        /> */}
        <LeafletMap
          originAirport={originAirport}
          destinationAirport={destinationAirport}
        />
      </Grid>
    </Grid>
  );
};
const StyledAirportAutocomplete = styled(AirportAutocomplete)`
  flex-basis: 40%;
`;
