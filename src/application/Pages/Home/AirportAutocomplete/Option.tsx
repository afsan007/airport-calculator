import { Grid, Stack, Typography } from '@mui/material';

interface OptionProps {
  name: string;
  iata: string;
  city: string;
  state: string;
  lat: string;
  lng: string;
  size: string;
}
export const Option = (props: OptionProps) => {
  return (
    <Stack sx={{ width: '100%', py: 1 }}>
      <Typography sx={{ fontWeight: 'bold', fontSize: '1.2rem' }}>
        {props.name}
      </Typography>
      <Grid
        container
        justifyContent='end'
        alignItems='center'
        sx={{ pt: 1 }}
        gap={3}
      >
        <Grid item>
          <Stack direction='row'>
            <Typography
              variant='subtitle2'
              sx={{ color: '#a5a3a3', fontWeight: 'bold', pr: 1 }}
            >
              City:
            </Typography>
            <Typography>{props.city}</Typography>
          </Stack>
        </Grid>
        <Grid item>
          <Stack direction='row'>
            <Typography
              variant='subtitle2'
              sx={{ color: '#a5a3a3', fontWeight: 'bold', pr: 1 }}
            >
              Iata:
            </Typography>
            <Typography>{props.iata}</Typography>
          </Stack>
        </Grid>
        <Grid item>
          <Stack direction='row'>
            <Typography
              variant='subtitle2'
              sx={{ color: '#a5a3a3', fontWeight: 'bold', pr: 1 }}
            >
              Size:
            </Typography>
            <Typography
              sx={{ textTransform: 'capitalize', fontWeight: 'bold' }}
            >
              {props.size.slice(0, 1)}
            </Typography>
          </Stack>
        </Grid>
      </Grid>
    </Stack>
  );
};
