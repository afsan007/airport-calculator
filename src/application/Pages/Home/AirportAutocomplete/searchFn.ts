import Fuse from 'fuse.js';
import { OptionType } from '$application/Pages/Home';
import usAirports from '../us_airports.json';

const searchOptions: Fuse.IFuseOptions<OptionType> = {
  includeScore: true,
  distance: 3,
  minMatchCharLength: 2,
  useExtendedSearch: true,
  keys: [
    { name: 'name', weight: 0.3 },
    { name: 'iata', weight: 0.5 },
    { name: 'city', weight: 0.1 },
    { name: 'state', weight: 0.1 },
  ],
};

const fuse = new Fuse(usAirports, searchOptions);

export const searchInOptions = (val: string): OptionType[] =>
  fuse.search(val).map((res) => res.item);
