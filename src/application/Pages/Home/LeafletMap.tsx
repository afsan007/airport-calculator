import {
  MapContainer,
  Marker,
  Polyline,
  TileLayer,
  Tooltip,
} from 'react-leaflet';
import { OptionType } from '../Home';
import 'leaflet/dist/leaflet.css';
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css';
import 'leaflet-defaulticon-compatibility';
import { Typography } from '@mui/material';
interface LeafletMapProps {
  originAirport?: OptionType;
  destinationAirport?: OptionType;
}
export const LeafletMap = ({
  originAirport,
  destinationAirport,
}: LeafletMapProps) => {
  return (
    <MapContainer
      center={[37.09024, -95.712891]}
      zoom={5}
      style={{
        height: '100%',
        minHeight: '400px',
        width: '100%',
        minWidth: '200px',
        boxSizing: 'border-box',
      }}
      doubleClickZoom={false}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      />
      {originAirport && (
        <Marker
          position={[Number(originAirport.lat), Number(originAirport.lng)]}
        >
          <Tooltip sticky>
            <Typography sx={{ fontWeight: 'bold' }}>
              {originAirport.name}
            </Typography>
          </Tooltip>
        </Marker>
      )}
      {destinationAirport && (
        <Marker
          position={[
            Number(destinationAirport.lat),
            Number(destinationAirport.lng),
          ]}
        >
          <Tooltip sticky>
            <Typography sx={{ fontWeight: 'bold' }}>
              {destinationAirport.name}
            </Typography>
          </Tooltip>
        </Marker>
      )}
      {destinationAirport && originAirport && (
        <Polyline
          pathOptions={{ color: 'red' }}
          positions={[
            [Number(destinationAirport.lat), Number(destinationAirport.lng)],
            [Number(originAirport.lat), Number(originAirport.lng)],
          ]}
        />
      )}
    </MapContainer>
  );
};

export default LeafletMap;
