import {
  Autocomplete,
  AutocompleteRenderInputParams,
  AutocompleteRenderOptionState,
  Box,
  TextField,
} from '@mui/material';
import { isEqual } from 'lodash-es';

import { OptionType } from '../Home';
import { Option } from './AirportAutocomplete/Option';
import { searchInOptions } from './AirportAutocomplete/searchFn';
import usAirports from './us_airports.json';

const renderOption: (
  props: React.HTMLAttributes<HTMLLIElement>,
  option: OptionType,
  state: AutocompleteRenderOptionState
) => React.ReactNode = (props, option) => (
  <Box
    component='li'
    key={option.state}
    {...props}
    sx={{
      my: 1,
    }}
  >
    <Option {...option} />
  </Box>
);

interface AirportAutocompleteProps {
  inputPlaceHolder: string;
  className?: string;
  disabledOption?: OptionType;
  onSelect: (selectedOption: OptionType) => void;
}
export const AirportAutocomplete = (props: AirportAutocompleteProps) => {
  const optionDisabledHandler = (val: OptionType) =>
    isEqual(val, props.disabledOption);

  const renderInput = (params: AutocompleteRenderInputParams) => (
    <TextField
      {...params}
      label={props.inputPlaceHolder}
      inputProps={params.inputProps}
    />
  );

  return (
    <Autocomplete
      getOptionLabel={(o) => o.name}
      className={props.className}
      options={usAirports}
      size='small'
      getOptionDisabled={optionDisabledHandler}
      filterOptions={(_, state) => searchInOptions(state.inputValue)}
      onChange={(_, val) => props.onSelect(val!)}
      groupBy={(options) => options.state}
      renderOption={renderOption}
      renderInput={renderInput}
    />
  );
};
