import { Status, Wrapper } from '@googlemaps/react-wrapper';
import { OptionType } from '../Home';
import Map from './GoogleMap/Map';

const render = (status: Status) => <h1>{status}</h1>;

interface GoogleMapProps {
  originAirport?: OptionType;
  destinationAirport?: OptionType;
}
export const GoogleMap = (props: GoogleMapProps) => (
  <Wrapper apiKey={process.env.GOOGLE_MAP_API_KEY!} render={render}>
    <Map {...props} />
  </Wrapper>
);

export default GoogleMap;
