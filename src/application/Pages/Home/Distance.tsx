import { Stack, Typography } from '@mui/material';
import { haversine } from '../../utils/harvsine';
import { OptionType } from '../Home';

interface DistanceProps {
  originAirport: OptionType;
  destinationAirport: OptionType;
}

export const Distance = (props: DistanceProps) => {
  return (
    <Stack
      direction='row'
      alignItems='center'
      justifyContent='center'
      sx={{
        py: 3,
        background: '#262626',
        color: '#fff',
        my: 2,
        borderRadius: 3,
        textTransform: 'capitalize',
      }}
      gap={2}
    >
      <Typography variant='h6'>Distance:: </Typography>
      <Typography variant='h4' sx={{ fontWeight: 'bold' }}>
        {haversine(
          Number(props.originAirport.lat),
          Number(props.originAirport.lng),
          Number(props.destinationAirport.lat),
          Number(props.destinationAirport.lng)
        )}
      </Typography>
      <Typography variant='h6'>nautical miles</Typography>
    </Stack>
  );
};
