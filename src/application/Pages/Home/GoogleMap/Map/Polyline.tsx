import { useEffect, useState } from 'react';
type PolylineProps = google.maps.PolylineOptions & {
  map: google.maps.Map;
  path: google.maps.LatLng[];
};
export const Polyline = (props: PolylineProps) => {
  const [path, setPath] = useState<google.maps.Polyline>();

  useEffect(() => {
    if (!path) {
      setPath(new google.maps.Polyline());
    }

    // remove path from map on unmount
    return () => {
      if (path) {
        path.setMap(null);
      }
    };
  }, [path]);

  useEffect(() => {
    if (path) {
      path.setOptions(props);
      // Resetting bounds
      const bounds = new google.maps.LatLngBounds();
      bounds.extend(props.path[0]);
      bounds.extend(props.path[1]);
      props.map.fitBounds(bounds);
    }
  }, [path, props]);

  return null;
};
