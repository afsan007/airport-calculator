import { useEffect, useRef, useState } from 'react';
import { OptionType } from '../../Home';
import { Marker } from './Map/Marker';
import { Polyline } from './Map/Polyline';

interface MapProps {
  originAirport?: OptionType;
  destinationAirport?: OptionType;
}

export const Map = (props: MapProps) => {
  const ref = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<google.maps.Map>();

  useEffect(() => {
    if (ref.current && !map) {
      setMap(
        new window.google.maps.Map(ref.current, {
          center: new window.google.maps.LatLng(37.09024, -95.712891),
          zoom: 3,
          mapTypeControl: false,
          streetViewControl: false,
        })
      );
    }
  }, [ref, map]);

  return (
    <>
      <div ref={ref} id='map' />
      {[props.originAirport, props.destinationAirport].map((point, idx) => {
        const position = new window.google.maps.LatLng(
          Number(point?.lat),
          Number(point?.lng)
        );
        return (
          <Marker
            key={idx}
            map={map}
            position={position}
            title={point?.name}
            label={idx === 0 ? 'A' : 'B'}
            animation={window.google.maps.Animation.DROP}
          />
        );
      })}
      {props.originAirport && props.destinationAirport && (
        <Polyline
          geodesic
          strokeColor='#FF0000'
          strokeOpacity={1.0}
          strokeWeight={2}
          map={map!}
          path={[
            new window.google.maps.LatLng(
              Number(props.originAirport?.lat),
              Number(props.originAirport?.lng)
            ),
            new window.google.maps.LatLng(
              Number(props.destinationAirport?.lat),
              Number(props.destinationAirport?.lng)
            ),
          ]}
        />
      )}
    </>
  );
};

export default Map;
